import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent {
  errorMessage = '';

  // Form Data
  formData = {
    emailInput: '',
    passwordInput: '',
  };

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit() {
    const email = this.formData.emailInput;
    const password = this.formData.passwordInput;
    this.errorMessage = '';
    console.log(this.formData);

    if (!password || !email) {
      this.errorMessage = 'All fields are required !';
      return;
    }

    this.authService.signIn(email, password).subscribe({
      next: (response) => {
        console.log(response);

        localStorage.setItem('token', response.token);
        this.router.navigate(['/']);
      },
      error: (error) => {
        console.error(error);
        this.errorMessage = 'Something went wrong !';
      },
      complete: () => {
        //complete() callback
        console.log('Request completed');
      },
    });
  }
}
