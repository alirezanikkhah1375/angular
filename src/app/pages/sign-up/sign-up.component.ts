import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent {
  errorMessage = '';

  // Form Data
  formData = {
    emailInput: '',
    passwordInput: '',
    confirmPasswordInput: '',
  };

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit() {
    const email = this.formData.emailInput;
    const password = this.formData.passwordInput;
    const confirmPassword = this.formData.confirmPasswordInput;
    this.errorMessage = '';

    if (!password || !confirmPassword || !email) {
      this.errorMessage = 'All fields are required !';
      return;
    }

    if (password !== confirmPassword) {
      this.errorMessage = 'Passwords do not match !';
      return;
    }

    this.authService.signUp(email, password).subscribe({
      next: (response) => {
        localStorage.setItem('token', response.token);
        this.router.navigate(['/']);
      },
      error: (error) => {
        console.log(error);
        this.errorMessage = 'Something went wrong !';
      },
      complete: () => {
        //complete() callback
        console.log('Request completed');
      },
    });
  }
}
