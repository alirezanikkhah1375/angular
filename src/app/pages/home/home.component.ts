import { Component, OnInit } from '@angular/core';
import { options } from 'src/app/helpers/consts';
import { NewsService } from 'src/app/services/news/news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  topic_options = options.topics;
  lang_options = options.languages;
  countries_options = options.countries;

  loading = false;
  errorMessage = '';
  topStories = [];
  searchTitle = '';

  // Form Data
  formData = {
    keyword: '',
    country: '',
    category: '',
    lang: '',
  };

  constructor(private newsServices: NewsService) {}

  ngOnInit(): void {
    this.loading = true;
    this.newsServices.searchHeadlines().subscribe({
      next: (response) => {
        //next() callback
        console.log(response);
        this.searchTitle = `Your search results : ${response.page_size} articles`;
        this.topStories = response.articles;
      },
      error: (error) => {
        //error() callback
        console.error('Request failed with error');
        this.errorMessage = error;
        this.loading = false;
      },
      complete: () => {
        //complete() callback
        console.log('Request completed');
        this.loading = false;
      },
    });
  }

  submitForm() {
    console.log(this.formData);
    this.newsServices
      .searchNews(
        this.formData.keyword,
        this.formData.lang,
        this.formData.category,
        this.formData.country
      )
      .subscribe({
        next: (response) => {
          console.log(response);
          if (response.page_size === 0) this.errorMessage = response.status;
          this.topStories = response.articles;
        },
        error: (error) => {
          console.log(error);
        },
        complete: () => {
          console.log('Subscription completed !');
        },
      });

    // reset from
    this.formData = {
      keyword: '',
      country: '',
      category: '',
      lang: '',
    };
  }
}
