import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.development';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  constructor(private http: HttpClient) {}
  apiURL = 'https://api.newscatcherapi.com/v2';

  searchHeadlines(
    lang: string = 'en',
    topic: string = 'politics',
    country: string = 'US',
    pageSize: number = 25
  ): Observable<any> {
    return this.http.get(`${this.apiURL}/latest_headlines`, {
      headers: {
        'x-api-key': environment.apiKey,
        'Access-Control-Allow-Origin': '*',
      },
      params: {
        lang: lang,
        topic: topic,
        countries: [country],
        page_size: pageSize,
      },
    });
  }

  searchNews(
    keyword: string,
    lang: string = 'en',
    topic: string = 'politics',
    country: string = 'US',
    pageSize: number = 25
  ): Observable<any> {
    return this.http.get(`${this.apiURL}/search`, {
      headers: {
        'x-api-key': environment.apiKey,
        'Access-Control-Allow-Origin': '*',
      },
      params: {
        q: keyword,
        lang: lang,
        topic: topic,
        countries: [country],
        page_size: pageSize,
      },
    });
  }

  getSources(
    lang: string = 'en',
    topic: string = 'politics',
    country: string = 'US'
  ): Observable<any> {
    return this.http.get(`${this.apiURL}/sources`, {
      headers: {
        'x-api-key': environment.apiKey,
        'Access-Control-Allow-Origin': '*',
      },
      params: {
        lang: lang,
        topic: topic,
        countries: [country],
      },
    });
  }
}
